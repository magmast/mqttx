import 'package:mqtt_client/mqtt_browser_client.dart';
import 'package:mqtt_client/mqtt_client.dart';

MqttClient createClient({
  required Uri uri,
  required String id,
}) =>
    MqttBrowserClient.withPort(
      Uri(scheme: uri.scheme, host: uri.host).toString(),
      id,
      uri.hasPort ? uri.port : 1883,
    );
