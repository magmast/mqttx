import 'package:collection/collection.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqttx/src/mqttx_qos.dart';
import 'package:mqttx/src/mqttx_qos_ext.dart';

class SubscriptionManager {
  final MqttClient client;
  final _subscriptions = <String, List<MqttxQos>>{};

  SubscriptionManager(this.client);

  void subscribe({required String topic, required MqttxQos qos}) {
    final bool shouldResubscribe = !_subscriptions.containsKey(topic) ||
        maxBy(_subscriptions[topic]!, (val) => val)! < qos;

    _subscriptions.update(
      topic,
      (value) => [...value, qos],
      ifAbsent: () => [qos],
    );

    if (shouldResubscribe) {
      client.subscribe(topic, qos.toMqtt());
    }
  }

  void unsubscribe({required String topic, required MqttxQos qos}) {
    if (!_subscriptions.containsKey(topic)) {
      return;
    }

    final shouldSendUnsubscribe = _subscriptions[topic]!.length == 1;
    if (shouldSendUnsubscribe) {
      _subscriptions.remove(topic);
      client.unsubscribe(topic);
      return;
    }

    _subscriptions.update(
      topic,
      (value) {
        final index = value.indexOf(qos);
        value.removeAt(index);
        return value;
      },
    );

    final maxQos = maxBy(_subscriptions[topic]!, (value) => value)!;

    if (maxQos < qos) {
      client.subscribe(topic, maxQos.toMqtt());
    }
  }
}
