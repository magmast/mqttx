import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqttx/src/mqttx_connection_state.dart';
import 'package:rxdart/rxdart.dart';

class ConnectionStateManager {
  ConnectionStateManager(MqttClient client) {
    client.onAutoReconnect =
        () => _subject.add(MqttxConnectionState.reconnecting);

    client.onAutoReconnected =
        () => _subject.add(MqttxConnectionState.connected);

    client.onConnected = () => _subject.add(MqttxConnectionState.connected);

    client.onDisconnected =
        () => _subject.add(MqttxConnectionState.disconnected);
  }

  final _subject = BehaviorSubject<MqttxConnectionState>();

  ValueStream<MqttxConnectionState> get stream => _subject.stream;

  Future<void> close() => _subject.close();
}
