import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqttx/src/mqttx_qos.dart';

extension MqttxQosExt on MqttxQos {
  MqttQos toMqtt() {
    switch (this) {
      case MqttxQos.atMostOnce:
        return MqttQos.atMostOnce;
      case MqttxQos.atLeastOnce:
        return MqttQos.atLeastOnce;
      case MqttxQos.exactlyOnce:
        return MqttQos.exactlyOnce;
    }
  }

  bool operator <(MqttxQos other) {
    return index < other.index;
  }
}
