import 'package:mqtt_client/mqtt_client.dart';

MqttClient createClient({
  required Uri uri,
  required String id,
}) =>
    throw UnsupportedError(
      'Cannot create client without "dart:html" or "dart:io" package.',
    );
