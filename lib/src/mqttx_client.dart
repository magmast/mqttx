import 'dart:async';

import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqttx/src/connection_state_manager.dart';
import 'package:mqttx/src/mqtt_client_builder.dart';
import 'package:mqttx/src/mqttx_connection_state.dart';
import 'package:mqttx/src/mqttx_last_will.dart';
import 'package:mqttx/src/mqttx_protocol_version.dart';
import 'package:mqttx/src/mqttx_qos.dart';
import 'package:mqttx/src/mqttx_qos_ext.dart';
import 'package:mqttx/src/mqttx_web_socket_protocols.dart';
import 'package:mqttx/src/subscription_manager.dart';
import 'package:rxdart/rxdart.dart';
import 'package:typed_data/typed_buffers.dart';

class MqttxClient {
  static Future<MqttxClient> connect({
    required Uri uri,
    required String id,
    bool autoReconnect = true,
    Duration keepAlive = const Duration(minutes: 1),
    List<String> websocketProtocols = MqttxWebSocketProtocols.single,
    String? username,
    String? password,
    bool cleanStart = true,
    MqttxProtocolVersion protocolVersion = MqttxProtocolVersion.v3_1_1,
    MqttxLastWill? lastWill,
  }) async {
    final client = MqttClientBuilder(
      uri: uri,
      id: id,
      autoReconnect: autoReconnect,
      keepAlive: keepAlive,
      websocketProtocols: websocketProtocols,
      username: username,
      password: password,
      cleanStart: cleanStart,
      protocolVersion: protocolVersion,
      lastWill: lastWill,
    ).build();

    await client.connect(username, password);

    return MqttxClient._(client: client);
  }

  MqttxClient._({required MqttClient client})
      : _client = client,
        _subscriptionManager = SubscriptionManager(client),
        _connectionStateManager = ConnectionStateManager(client);

  final MqttClient _client;

  final SubscriptionManager _subscriptionManager;

  final ConnectionStateManager _connectionStateManager;

  ValueStream<MqttxConnectionState> get state => _connectionStateManager.stream;

  Future<void> publish({
    bool retain = false,
    MqttxQos qos = MqttxQos.atMostOnce,
    required String topic,
    required List<int> message,
  }) async {
    final buffer = Uint8Buffer();
    buffer.addAll(message);

    final id = _client.publishMessage(
      topic,
      qos.toMqtt(),
      buffer,
      retain: retain,
    );

    if (qos != MqttxQos.atMostOnce) {
      await _client.published!
          .firstWhere((msg) => msg.variableHeader?.messageIdentifier == id);
    }
  }

  /// Subscribes to a [topic]. Returns a [Stream] of messages published to the
  /// [topic]. Cancel the subscription by canceling the returned [Stream].
  Stream<List<int>> subscribe(
    String topic, {
    MqttxQos qos = MqttxQos.atMostOnce,
  }) {
    _subscriptionManager.subscribe(topic: topic, qos: qos);

    return _client.updates!
        .expand((messages) => messages)
        .where((message) => message.topic == topic)
        .map((message) => message.payload)
        .whereType<MqttPublishMessage>()
        .map((message) {
      final mqttBuffer = MqttByteBuffer(null);
      message.payload.writeTo(mqttBuffer);
      return mqttBuffer.buffer!.toList();
    }).doOnCancel(
      () => _subscriptionManager.unsubscribe(topic: topic, qos: qos),
    );
  }

  void disconnect() => _client.disconnect();

  /// Disposes all resources allocated by the [MqttxClient]. You cannot use this
  /// client after calling this method.
  ///
  /// This method does only what previous paragraph says. It does not
  /// disconnects from the broker in the MQTT meaning of the word "disconnect"
  /// i.e. it closes the underlying connection, but does not send DISCONNECT
  /// control packet.
  Future<void> close() => _connectionStateManager.close();
}
