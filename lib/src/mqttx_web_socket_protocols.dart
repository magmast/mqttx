import 'package:mqtt_client/mqtt_client.dart';

class MqttxWebSocketProtocols {
  static const single = MqttClientConstants.protocolsSingleDefault;
  static const multiple = MqttClientConstants.protocolsMultipleDefault;
}
