enum MqttxQos {
  atMostOnce,
  atLeastOnce,
  exactlyOnce,
}
