enum MqttxConnectionState {
  connected,
  disconnected,
  reconnecting,
}
