import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqttx/src/mqttx_last_will.dart';
import 'package:mqttx/src/mqttx_protocol_version.dart';
import 'package:mqttx/src/mqttx_qos_ext.dart';
import 'package:mqttx/src/mqttx_web_socket_protocols.dart';

import 'create_client_stub.dart'
    if (dart.library.io) 'package:mqttx/src/create_client_server.dart'
    if (dart.library.html) 'package:mqttx/src/create_client_browser.dart';

class MqttClientBuilder {
  const MqttClientBuilder({
    required this.uri,
    required this.id,
    this.protocolVersion = MqttxProtocolVersion.v3_1_1,
    this.keepAlive = const Duration(minutes: 1),
    this.cleanStart = true,
    this.username,
    this.password,
    this.lastWill,
    this.autoReconnect = true,
    this.websocketProtocols = MqttxWebSocketProtocols.single,
    this.logging = false,
  });

  final Uri uri;
  final String id;
  final MqttxProtocolVersion protocolVersion;
  final Duration keepAlive;
  final bool cleanStart;
  final String? username;
  final String? password;
  final MqttxLastWill? lastWill;
  final bool autoReconnect;
  final List<String> websocketProtocols;
  final bool logging;

  MqttClient build() => createClient(uri: uri, id: id)
    ..autoReconnect = autoReconnect
    ..keepAlivePeriod = keepAlive.inSeconds
    ..websocketProtocols = websocketProtocols
    ..logging(on: logging)
    ..connectionMessage = _buildConnectMessage();

  MqttConnectMessage _buildConnectMessage() {
    final connectMessage = MqttConnectMessage()
        .withClientIdentifier(id)
        .withProtocolName(
          protocolVersion == MqttxProtocolVersion.v3_1_1
              ? MqttClientConstants.mqttV311ProtocolName
              : MqttClientConstants.mqttV31ProtocolName,
        )
        .withProtocolVersion(
          protocolVersion == MqttxProtocolVersion.v3_1_1
              ? MqttClientConstants.mqttV311ProtocolVersion
              : MqttClientConstants.mqttV31ProtocolVersion,
        );

    if (username != null) {
      connectMessage.authenticateAs(username, password);
    }

    if (cleanStart) {
      connectMessage.startClean();
    }

    if (lastWill != null) {
      connectMessage
          .withWillQos(lastWill!.qos.toMqtt())
          .withWillMessage(lastWill!.message)
          .withWillTopic(lastWill!.topic);
      if (lastWill!.retain) {
        connectMessage.withWillRetain();
      }
    }

    return connectMessage;
  }
}
