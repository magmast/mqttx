import 'package:mqttx/src/mqttx_qos.dart';

class MqttxLastWill {
  final MqttxQos qos;
  final String topic;
  final String message;
  final bool retain;

  const MqttxLastWill({
    this.qos = MqttxQos.atMostOnce,
    required this.topic,
    required this.message,
    this.retain = false,
  });
}
