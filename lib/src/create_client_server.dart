import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

MqttClient createClient({
  required Uri uri,
  required String id,
}) {
  final isWebSocket = ['ws', 'wss'].contains(uri.scheme);

  final client = MqttServerClient.withPort(
    isWebSocket ? Uri(scheme: uri.scheme, host: uri.host).toString() : uri.host,
    id,
    uri.hasPort ? uri.port : 1883,
  );

  if (isWebSocket) {
    client.useWebSocket = true;
  }

  return client;
}
