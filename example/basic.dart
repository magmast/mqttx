import 'dart:convert';
import 'dart:isolate';

import 'package:mqttx/mqttx.dart';

void main() async {
  final port = ReceivePort();

  Isolate.spawn(publisher, port.sendPort);
  Isolate.spawn(subscriber, port.sendPort);

  await port.take(2).drain();
}

void publisher(SendPort port) async {
  final client = await MqttxClient.connect(
    uri: Uri(host: 'test.mosquitto.org'),
    id: 'mqttx-example-basic-publisher',
  );

  for (var i = 0; i < 10; i++) {
    await client.publish(
      topic: 'mqttx/example/basic/topic',
      message: utf8.encode('Hello, World!'),
      retain: true,
    );
  }

  port.send(null);
}

void subscriber(SendPort port) async {
  final client = await MqttxClient.connect(
    uri: Uri(host: 'test.mosquitto.org'),
    id: 'mqttx-example-basic-subscriber',
  );

  await client
      .subscribe('mqttx/example/basic/topic')
      .take(10)
      .forEach((message) {
    print(utf8.decode(message));
  });

  port.send(null);
}
